﻿using AutoMapper;
using Newtonsoft.Json;
using OddestOdds.Entities;
using OddestOdds.Entities.DTOs;
using OddestOdds.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Logic
{
    public class OddsManager : IOddsManager
    {
        public MapperConfiguration ConfigMapper { get{ return configMapper; } }

        private MapperConfiguration configMapper = new MapperConfiguration(cfg => {
            cfg.CreateMap<Odd, OddDTO>();
        });

        private MapperConfiguration configMapperReversed = new MapperConfiguration(cfg => {
            cfg.CreateMap<OddDTO, Odd>();
        });

        private OddestOddsEntities oddsContext;

        public OddsManager(OddestOddsEntities oddsContext)
        {
            this.oddsContext = oddsContext ?? new OddestOddsEntities();
        }

        public string GetLatestJsonOdds()
        {
            return JsonConvert.SerializeObject(GetLatestOdds());
        }

        public List<OddDTO> GetLatestOdds()
        {
            var oddsDTOs = new List<OddDTO>();

            IMapper mapper = configMapper.CreateMapper();

            //Get the odds from the Database
            var allOdds = oddsContext.Odds;
            foreach (Odd anOdd in oddsContext.Odds.ToList())
            {
                var oddDTO = mapper.Map<Odd, OddDTO>(anOdd);
                oddDTO.Match = $"{ anOdd.Match.Team1.TeamName} VS { anOdd.Match.Team.TeamName}";

                oddsDTOs.Add(oddDTO);
            }

            return oddsDTOs;
        }

        public void DeleteOdd(int matchId)
        {
            var oddToDelete = new Odd() { MatchId = matchId };

            oddsContext.SetDeleted(oddToDelete);
            oddsContext.SaveChanges();
        }

        public void AddOdd(OddDTO oddToAdd)
        {
            var odd = new Odd()
            {
                MatchId = oddToAdd.MatchId,
                HomeWin = oddToAdd.HomeWin,
                AwayWin = oddToAdd.AwayWin,
                Draw = oddToAdd.Draw
            };

            oddsContext.Odds.Add(odd);
            oddsContext.SaveChanges();
        }

        public void UpdateOdd(int matchId, OddDTO oddToUpdate)
        {
            var currentOdd = oddsContext.Odds.Find(matchId);
            if (currentOdd != null)
            {
                currentOdd.HomeWin = oddToUpdate.HomeWin;
                currentOdd.Draw = oddToUpdate.Draw;
                currentOdd.AwayWin = oddToUpdate.AwayWin;
                oddsContext.SaveChanges();
            }
        }

        public void Dispose()
        {
            oddsContext?.Dispose();

            //Setting dbContext to null so as to avoid double disposes errors.
            oddsContext = null;
        }
    }
}
