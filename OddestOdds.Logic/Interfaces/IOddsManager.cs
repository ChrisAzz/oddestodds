﻿using AutoMapper;
using OddestOdds.Entities;
using OddestOdds.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Logic.Interfaces
{
    /// <summary>
    /// Interface to Odds Manager implementations
    /// </summary>
    public interface IOddsManager : IDisposable
    {
        MapperConfiguration ConfigMapper { get; }

        /// <summary>
        /// Gets the latest Odds from the DataSource in JSON format
        /// </summary>
        /// <returns>A list of Odds in JSON format</returns>
        string GetLatestJsonOdds();

        /// <summary>
        /// Gets a list of latest Odds from the DataSource in DTO format
        /// </summary>
        /// <returns>A list of Odds in DTO format</returns>
        List<OddDTO> GetLatestOdds();

        /// <summary>
        /// Deletes an Odd from the DataSource
        /// </summary>
        /// <param name="matchId"></param>
        void DeleteOdd(int matchId);

        /// <summary>
        /// Adds an Odd to the DataSource
        /// </summary>
        /// <param name="oddToAdd"></param>
        void AddOdd(OddDTO oddToAdd);

        /// <summary>
        /// Updates an Odd in the DataSource
        /// </summary>
        /// <param name="matchId"></param>
        /// <param name="oddToUpdate"></param>
        void UpdateOdd(int matchId, OddDTO oddToUpdate);
    }
}
