﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.Entities.DTOs
{
    /// <summary>
    /// Class for an Odd DTO object
    /// </summary>
    public class OddDTO
    {
        public int MatchId { get; set; }
        public string Match { get; set; } //Match friendly description 
        public decimal HomeWin { get; set; }
        public decimal Draw { get; set; }
        public decimal AwayWin { get; set; }

        /// <summary>
        /// Overrides the Equals method to be used during unit-tests
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj is OddDTO)
            {
                var otherOddDto = obj as OddDTO;

                return MatchId == otherOddDto.MatchId &&
                       HomeWin == otherOddDto.HomeWin &&
                       Draw == otherOddDto.Draw &&
                       AwayWin == otherOddDto.AwayWin;
            }

            return false;
        }
    }
}
