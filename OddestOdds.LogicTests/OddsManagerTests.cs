﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using OddestOdds.Entities;
using OddestOdds.Entities.DTOs;
using OddestOdds.Logic.Interfaces;
using OddestOdds.LogicTests.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace OddestOdds.Logic.Tests
{
    [TestClass()]
    public class OddsManagerTests
    {
        [TestMethod()]
        public void LatestOddsFromDbGeneratesCorrectJson()
        {
            //Testing that the odds in DB are reflected correctly as JSON
            using (OddestOddsEntities oddestOddsContext = new OddestOddsEntities())
            {
                IOddsManager oddsManager = new OddsManager(oddestOddsContext);
                IMapper mapper = oddsManager.ConfigMapper.CreateMapper();

                var oddsInDB = oddestOddsContext.Odds.ToList();
                var oddsInJson = oddsManager.GetLatestJsonOdds();

                //Deserialize JSON so that we can compare to what there is in the database
                List<OddDTO> jsonOddsList = (List<OddDTO>) JsonConvert.DeserializeObject(oddsInJson, typeof(List<OddDTO>));

                //Test that the number of JSON objects are the same as the number of Odds in the DB
                Assert.AreEqual(oddsInDB.Count, jsonOddsList.Count);

                //Test that the JSON items are what there is in the data-source
                foreach(Odd anOddInDb in oddsInDB.ToList())
                {
                    var jsonOdd = jsonOddsList.FirstOrDefault(x => x.MatchId == anOddInDb.MatchId);
                    if(jsonOdd is null)
                    {
                        Assert.Fail("Odd in DB not found in generated JSON");
                    }
                    else
                    {
                        OddDTO anOddInDbAsDTO = mapper.Map<Odd, OddDTO>(anOddInDb);
                        Assert.AreEqual(jsonOdd, anOddInDbAsDTO);
                    }
                }
            }
        }

        [TestMethod()]
        public void AddOddCallsCoreEFMethods()
        {
            //Mocking the 'Odds' table
            var oddSet = new Mock<DbSet<Odd>>();

            //Mocking the DB Context
            var oddsContextMock = new Mock<OddestOddsEntities>();

            //'Assigning' the mocked Odds table to the fake context.
            oddsContextMock.Setup(o => o.Odds).Returns(oddSet.Object);

            OddDTO oddToAdd = new OddDTO
            {
                Match = "Test",
                MatchId = 10,
                HomeWin = 1.43M,
                Draw = 2.45M,
                AwayWin = 1.43M
            };

            using (IOddsManager oddsManager = new OddsManager(oddsContextMock.Object))
            {
                oddsManager.AddOdd(oddToAdd);

                //Test that the 'Add' method has been called on the Odds table once.
                oddSet.Verify(o => o.Add(It.IsAny<Odd>()), Times.Once());

                //Test that the changes are being committed to the data-source.
                oddsContextMock.Verify(o => o.SaveChanges(), Times.Once());
            }
        }

        [TestMethod()]
        public void DeleteOddCallsCoreEFMethods()
        {
            //Mocking the 'Odds' table
            var oddSet = new Mock<DbSet<Odd>>();

            //Mocking the DB Context
            var oddsContextMock = new Mock<OddestOddsEntities>();

            //'Assigning' the mocked Odds table to the fake context.
            oddsContextMock.Setup(o => o.Odds).Returns(oddSet.Object);

            using (IOddsManager oddsManager = new OddsManager(oddsContextMock.Object))
            {
                var matchId = 10;

                oddsManager.DeleteOdd(matchId);

                //Check that the odd has been flagged for deletion
                oddsContextMock.Verify(o => o.SetDeleted(It.IsAny<Odd>()), Times.AtLeastOnce());

                //Test that the changes are being committed to the data-source.
                oddsContextMock.Verify(o => o.SaveChanges(), Times.Once());
            }
        }

        [TestMethod()]
        public void UpdateOddCallsCoreEFMethods()
        {
            var oddFakeData = new List<Odd>()
            {
                new Odd(){MatchId=10, HomeWin=2.32M, Draw=1.42M, AwayWin=3.22M }
            }.AsQueryable();

            //Mocking the 'Odds' table and make it queryable
            var oddSet = new Mock<DbSet<Odd>>();
            MoqHelper.MockDbSetAsIQueryable(ref oddSet, ref oddFakeData);
            oddSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => oddFakeData.FirstOrDefault(d => d.MatchId == (int)ids[0]));

            //Mocking the DB Context
            var oddsContextMock = new Mock<OddestOddsEntities>();

            //'Assigning' the mocked Odds table to the fake context.
            oddsContextMock.Setup(o => o.Odds).Returns(oddSet.Object);

            using (IOddsManager oddsManager = new OddsManager(oddsContextMock.Object))
            {
                var matchId = 10;

                var modifiedOdd = new OddDTO()
                {
                    MatchId = matchId,
                    HomeWin = 1.44M,
                    Draw = 2.46M,
                    AwayWin = 1.43M
                };

                oddsManager.UpdateOdd(matchId, modifiedOdd);

                //Check that the odd to be updated has been queried from the Database
                oddSet.Verify(o => o.Find(It.IsAny<int>()), Times.AtLeastOnce());

                //Test that the changes are being committed to the data-source.
                oddsContextMock.Verify(o => o.SaveChanges(), Times.Once());
            }
        }

        [TestMethod()]
        public void AddOddAdds1ItemToListOfOdds()
        {
            var oddFakeData = new List<Odd>()
            {
                new Odd(){MatchId=10, HomeWin=2.32M, Draw=1.42M, AwayWin=3.22M }
            };

            var previousCount = oddFakeData.Count();

            //Mocking the 'Odds' table and mock the 'Add' method to add to our fake data-source
            var oddSet = new Mock<DbSet<Odd>>();
            oddSet.Setup(d => d.Add(It.IsAny<Odd>())).Callback<Odd>((s) => oddFakeData.Add(s));

            //Mocking the DB Context
            var oddsContextMock = new Mock<OddestOddsEntities>();

            //'Assigning' the mocked Odds table to the fake context.
            oddsContextMock.Setup(o => o.Odds).Returns(oddSet.Object);

            using (IOddsManager oddsManager = new OddsManager(oddsContextMock.Object))
            {
                OddDTO oddToAdd = new OddDTO
                {
                    Match = "Test",
                    MatchId = 11,
                    HomeWin = 2.43M,
                    Draw = 1.26M,
                    AwayWin = 2.33M
                };

                oddsManager.AddOdd(oddToAdd);

                //Check that the odd count increased by 1
                Assert.AreEqual(previousCount + 1, oddFakeData.Count());

                //Check that our new Odd is there
                Assert.IsTrue(oddFakeData.Any(x => x.MatchId == oddToAdd.MatchId && 
                                                   x.HomeWin == oddToAdd.HomeWin && 
                                                   x.Draw == oddToAdd.Draw && 
                                                   x.AwayWin == oddToAdd.AwayWin));
            }
        }

        [TestMethod()]
        public void DeleteOddDeletesExistingItemFromListOfOdds()
        {
            var oddFakeData = new List<Odd>()
            {
                new Odd(){MatchId=10, HomeWin=2.32M, Draw=1.42M, AwayWin=3.22M },
                new Odd(){MatchId=13, HomeWin=3.31M, Draw=1.44M, AwayWin=1.36M }
            };

            var previousCount = oddFakeData.Count();

            //Mocking the 'Odds' table 
            var oddSet = new Mock<DbSet<Odd>>();

            //Mocking the DB Context
            var oddsContextMock = new Mock<OddestOddsEntities>();

            //'Assigning' the mocked Odds table to the fake context. Mocked the 'SetDeleted' function in order to remove the Odd from our fake list
            oddsContextMock.Setup(o => o.Odds).Returns(oddSet.Object);
            oddsContextMock.Setup(o => o.SetDeleted(It.IsAny<Odd>())).Callback<Odd>((s) => oddFakeData.RemoveAll(x=>x.MatchId == s.MatchId));

            using (IOddsManager oddsManager = new OddsManager(oddsContextMock.Object))
            {
                var oddToDelete = 10;

                oddsManager.DeleteOdd(oddToDelete);

                //Check that the odd count decreased by 1
                Assert.AreEqual(previousCount - 1 , oddFakeData.Count());

                //Check that the deleted Odd is NOT there
                Assert.IsTrue(!oddFakeData.Any(x => x.MatchId == oddToDelete));
            }
        }

        [TestMethod()]
        public void UpdateOddUpdatesExistingItemInListOfOdds()
        {
            var matchIdToUpdate = 10;

            var oddFakeData = new List<Odd>()
            {
                new Odd(){MatchId=10, HomeWin=2.32M, Draw=1.42M, AwayWin=3.22M },
                new Odd(){MatchId=13, HomeWin=3.31M, Draw=1.44M, AwayWin=1.36M }
            }.AsQueryable();

            var previousCount = oddFakeData.Count();

            //Mocking the 'Odds' table 
            var oddSet = new Mock<DbSet<Odd>>();
            MoqHelper.MockDbSetAsIQueryable(ref oddSet, ref oddFakeData);
            oddSet.Setup(m => m.Find(It.IsAny<object[]>())).Returns<object[]>(ids => oddFakeData.FirstOrDefault(d => d.MatchId == (int)ids[0]));

            //Mocking the DB Context
            var oddsContextMock = new Mock<OddestOddsEntities>();

            //'Assigning' the mocked Odds table to the fake context. 
            oddsContextMock.Setup(o => o.Odds).Returns(oddSet.Object);

            using (IOddsManager oddsManager = new OddsManager(oddsContextMock.Object))
            {
                var oddToUpdate = new OddDTO() {
                    Match = "Test",
                    MatchId = matchIdToUpdate,
                    HomeWin = 1.11M,
                    Draw = 2.22M,
                    AwayWin = 3.33M
                };

                oddsManager.UpdateOdd(oddToUpdate.MatchId, oddToUpdate);

                //Check that the odd count remained the SAME
                Assert.AreEqual(previousCount, oddFakeData.Count());
            }
        }
    }
}