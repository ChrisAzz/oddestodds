﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.LogicTests.Helpers
{
    public static class MoqHelper
    {
        public static void MockDbSetAsIQueryable<T>(ref Mock<DbSet<T>> set, ref IQueryable<T> list) where T : class
        {
            set.As<IQueryable<T>>().Setup(o => o.Provider).Returns(list.Provider);
            set.As<IQueryable<T>>().Setup(o => o.Expression).Returns(list.Expression);
            set.As<IQueryable<T>>().Setup(o => o.ElementType).Returns(list.ElementType);
            set.As<IQueryable<T>>().Setup(o => o.GetEnumerator()).Returns(list.GetEnumerator());
        }
    }
}
