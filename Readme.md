Oddest Odds Website & Back-Office
--------------------------------------

This project is intended as a Proof of Concept (PoC) for a web-site that displays odds in real-time to customers. This project is mainly split two-fold into a public section which any user can access, and a private back-office section where only odd handlers can log-in to and manage the odds. 


Getting Started
-------------------

To get a copy of the source code, one can download it via BitBucket at the following location: https://bitbucket.org/ChrisAzz/oddestodds/ 

A copy of the databases (ASP Authorization and OddestOdds DB) can be obtained in MDF format by following the link https://www.dropbox.com/sh/7gg2zh3185gzqpm/AADBlMzOs-ffpAJaRIobBmCQa?dl=0

As an administrator username/password one can use test@igamingcloud.com with password Test!Pass1530 


Installing
-----------

To run the project, kindly follow the following steps:

1. Download and Install Visual Studio 2017 (community edition works fine). This will automatically install the SQL Server Local DB version, which is enough to run this application.

2. Open the solution with VS 2017. Since Nuget is used throughout the project, it should take care of downloading the dependencies by itself.

3. Download and Install Microsoft SQL Server Management Studio in order to be able to import the databases into (LocalDb)\MSSQLLocalDB.

4. Open SQL Server Management Studio and connect to (LocalDb)\MSSQLLocalDB instance.

5. Use the "Attach" feature and add the databases to the instance. Follow this link for instructions on how to attach a database: https://www.devart.com/dbforge/sql/studio/copy-database.html

6. Go back to the solution in Visual Studio, and make sure that in the Web.Config the 'DefaultConnection' and the 'OddestOddsEntites' point to the correct databases that you just imported.

7. Build and run the solution.


Technologies Used
------------------------

- ASP.NET (C#) with MVC
- Knockout.js for MVVM
- Web API for REST Back-Office calls
- SignalR for web-socket real-time updates of odds data
- Entity Framework as the ORM
- MSTest Unit Testing from Visual Studio & MOQ for mocking the EF context.
- SQL Server Local DB as the database
- Auto-generated OWIN component authorization for log-in purposes.
- AutoMapper for mapping between Entity Framework Models and DTOs (Data Transfer Objects)

Versioning
-------------

Used GitHub as a versioning tool, with BitBucket as the repository holder.


How to Run Unit Tests
----------------------------

Unit tests can be run directly from Visual Studio by using the Test Explorer, or else MSTest.exe can be used independently through command-line outside the IDE. For further information on how to do it via command-line, please follow this link: https://msdn.microsoft.com/en-us/library/ms182487.aspx


How to Use the system
-----------------------------

1. Running the solution should open up a browser and show immediately the front-page with a table of odds. Internally, these odds were fetched via SignalR from the Database. Leave the current window as is at the front-page. This will simulate a public user looking at the real-time odds. Let's call this instance PUBLIC-SITE (PS).

2. Open another tab (or better yet a new window so that you can see both at the same time) of the browser and copy / paste the URL of the web-site. This new window will simulate an odds handler that has gone in the site to manage the odds. Let's call this instance BACKOFFIC-SITE (BS). 

3. At the BS, log-in as an odds handler with the username and password provided.

4. Click the "Backoffice" link at the top bar and the system will take you to the back-office section, with a table similar to that on the front-page but with editing functionality like 'Delete', 'Add New' or 'Update'.

5. Do the modifications you require and see the changes be reflected in the BS table of odds.

6. Finally, make sure you note the state of the PS grid in that it is still showing the old version of the odds data (since the public user did not do any refreshes). Then, go to the BS and click 'PUBLISH' and note how the table at the PS side is automatically updated without any refreshes of the page. 



