﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OddestOdds.Controllers
{
    [System.Web.Mvc.Authorize]
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BackOffice()
        {
            ViewBag.Message = "Here you can modify the current odds. Click 'PUBLISH' in order to push the new odds to the present viewers.";

            return View();
        }

        /// <summary>
        /// Controller method that is called whenever the odds need to be pushed to live users.
        /// </summary>
        public void Publish()
        {
            RealTimeHub.PublishOddsView();
        }
    }
}