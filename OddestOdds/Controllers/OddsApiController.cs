﻿using OddestOdds.Entities;
using OddestOdds.Entities.DTOs;
using OddestOdds.Logic;
using OddestOdds.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OddestOdds.Controllers
{
    [Authorize]
    public class OddsApiController : ApiController
    {
        private IOddsManager oddManager = new OddsManager(new OddestOddsEntities());

        // GET: api/OddsApi
        public IEnumerable<OddDTO> Get()
        {
            return oddManager.GetLatestOdds();
        }

        // POST: api/OddsApi
        public void Post([FromBody] OddDTO newOdd)
        {
            oddManager.AddOdd(newOdd);
        }

        // PUT: api/OddsApi/{MatchId}
        public void Put(int id, [FromBody] OddDTO updatedOdd)
        {
            oddManager.UpdateOdd(id, updatedOdd);
        }

        // DELETE: api/OddsApi/{MatchId}
        public void Delete(int id)
        {
            oddManager.DeleteOdd(id);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                oddManager.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
