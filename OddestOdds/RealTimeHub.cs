﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Text;
using OddestOdds.Entities;
using OddestOdds.Logic.Interfaces;
using OddestOdds.Logic;

namespace OddestOdds
{
    //This class provides real-time communication to clients via SignalR web-sockets
    public class RealTimeHub : Hub
    {
        /// <summary>
        /// Method to be called when the client needs to get the latest odds directly from the Database.
        /// </summary>
        public void GetOddsViewFromDb()
        {
            using (IOddsManager oddsManager = new OddsManager(new OddestOddsEntities()))
            {
                //Call Logic here to fetch the current odds from the database.
                Clients.All.UpdateOddsView(oddsManager.GetLatestJsonOdds());
            }
        }

        /// <summary>
        /// Method to be called whenever the odds need to be published to all the clients connected to SignalR hub.
        /// </summary>
        public static void PublishOddsView()
        {
            using (IOddsManager oddsManager = new OddsManager(new OddestOddsEntities()))
            {
                var hubContext = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();
                hubContext.Clients.All.UpdateOddsView(oddsManager.GetLatestJsonOdds());
            }
        }
    }
}