﻿var oddsEditorViewModel = function () {

    var self = this;
    self.MatchId = ko.observable();
    self.HomeWin = ko.observable();
    self.Draw = ko.observable();
    self.AwayWin = ko.observable();

    self.Odds = ko.observableArray([]);
    loadOdds();

    //Clears the KO observable fields 
    function clearFields() {
        self.MatchId('');
        self.HomeWin('');
        self.Draw('');
        self.AwayWin('');
    }

    //Calls the GET REST Api to fetch all the current bets, then assigns them to the observable array "Odds"
    function loadOdds() {
        $.ajax({
            type: "GET",
            url: "/api/OddsApi"
        }).done(function (data) {
            self.Odds(data);
        }).fail(function (err) {
            alert(err.status);
        });
    };

    //Calls the DELETE REST Api to delete an odd based on the Match Id
    deleteOdd = function (odd) {
        $.ajax({
            type: "DELETE",
            url: "/api/OddsApi/" + odd.MatchId
        }).done(function (data) {
            loadOdds();
        }).fail(function (err) {
            alert(err.status);
        });
    }

    //Stringifies the KO fields (making up a new odd) into JSON and calls the POST REST Api to add it into the DB
    addOdd = function () {
        var NewOdd = {
            MatchId: self.MatchId(),
            HomeWin: self.HomeWin(),
            Draw: self.Draw(),
            AwayWin: self.AwayWin()
        };  

        $.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/api/OddsApi/',
            data: JSON.stringify(NewOdd),
            success: function () {
                loadOdds();
                clearFields();
                $('#addOddModal').modal('hide');
            }
        });
    }

    //Gets the selected odd from the bound grid record and stores the current values to the observables for editing
    getOddToUpdate = function (odd) {
        self.MatchId(odd.MatchId);
        self.HomeWin(odd.HomeWin);
        self.Draw(odd.Draw);
        self.AwayWin(odd.AwayWin);

        $('#updateOddModal').modal('show');
    }

    //Stringifies the KO fields (making up the updated odd) into JSON and calls the PUT REST Api to update it in the DB
    updateOdd = function () {
        var UpdatedOdd = {
            MatchId: self.MatchId(),
            HomeWin: self.HomeWin(),
            Draw: self.Draw(),
            AwayWin: self.AwayWin()
        };

        $.ajax({
            type: 'PUT',
            contentType: "application/json",
            url: '/api/OddsApi/' + self.MatchId(),
            data: JSON.stringify(UpdatedOdd),
            success: function () {
                loadOdds();
                $('#updateOddModal').modal('hide');
            }
        }).always(function () {
            clearFields();
        });
    }
};

ko.applyBindings(new oddsEditorViewModel());  