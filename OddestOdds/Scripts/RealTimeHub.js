﻿var oddsViewModel = {
    Odds: ko.observableArray()
};

$.connection.hub.start()
    .done(function () {
        console.log('Successfully connected to SignalR hub...');

        //Publish odds immediately
        $.connection.realTimeHub.server.getOddsViewFromDb();
    })
    .fail(function () { alert('Connection to update Hub failed! Odds will not be updated.'); });

//Function to be called on each client when the Hub's updateOddsView method is triggered.
$.connection.realTimeHub.client.updateOddsView =
    function (oddsInJson) {
        oddsViewModel.Odds.removeAll(); //Remove previous version of the odds.
        var odds = jQuery.parseJSON(oddsInJson);
        for (var i = 0, len = odds.length; i < len; i++) {
            oddsViewModel.Odds.push(odds[i]);
        }
        ko.applyBindings(oddsViewModel);
    };