﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OddestOdds.Startup))]
namespace OddestOdds
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
